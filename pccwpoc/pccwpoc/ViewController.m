//
//  ViewController.m
//  pccwpoc
//
//  Created by Chathura Palihakkara on 16/7/20.
//  Copyright © 2020 PCCW. All rights reserved.
//

#import "ViewController.h"
#import "UserViewController.h"

@interface ViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UIButton *btnEnter;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (IBAction)pressEnter:(id)sender {
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    _btnEnter.hidden = newLength < 3 ;
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"userdetailsegue"])
    {
        UserViewController *vc = [segue destinationViewController];
        [vc setUsername:self.txtName.text];
    }
}

@end
