//
//  AppDelegate.m
//  pccwpoc
//
//  Created by Chathura Palihakkara on 16/7/20.
//  Copyright © 2020 PCCW. All rights reserved.
//

#import "AppDelegate.h"
#import "UserViewController.h"

@interface AppDelegate ()
- (void)navigateToViewCon:(NSString *)screen openWithValue:(NSString *)value;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    NSLog(@" App is lanuching ");
    
    return YES;
}

- (BOOL)application:(UIApplication *)application
continueUserActivity:(NSUserActivity *)userActivity
 restorationHandler:(void (^)(NSArray<id<UIUserActivityRestoring>> *restorableObjects))restorationHandler{
    NSLog(@" continueUserActivity called.. ");
    if ([[userActivity activityType] isEqualToString:NSUserActivityTypeBrowsingWeb])
    {
        NSURL *incomingURL = userActivity.webpageURL;
        NSLog(@"incomingURL %@",incomingURL);
        if (incomingURL) {
            NSURLComponents* urlComponents = [NSURLComponents componentsWithURL:incomingURL resolvingAgainstBaseURL:YES];
            if (urlComponents) {
                NSString *host = urlComponents.host;
                if ([host isEqualToString:@"adampccw.github.io"]) {
                    NSString *path = urlComponents.path;
                    NSArray *params = urlComponents.queryItems;
                    if (path && params) {
                        if ([path isEqualToString:@"/users/"] && params.count > 0) {
                            for (NSURLQueryItem* item  in params) {
                                if ([item.name isEqualToString:@"UserName"]){
                                    [self navigateToViewCon:@"userDetailScreen" openWithValue:item.value];
                                    break;
                                }
                            }
                        }
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
    return false;
}

- (void)navigateToViewCon:(NSString *)screen openWithValue:(NSString *)value {
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigationController = (UINavigationController *)_window.rootViewController;
    if ([screen isEqualToString:@"userDetailScreen"]) {
        UserViewController *userdetailvc = [storyBoard instantiateViewControllerWithIdentifier:@"userDetailScreen"];
        [userdetailvc setUsername:value];
        [navigationController pushViewController:userdetailvc animated:YES];
    }
}



@end
