//
//  UserViewController.m
//  pccwpoc
//
//  Created by Chathura Palihakkara on 16/7/20.
//  Copyright © 2020 PCCW. All rights reserved.
//

#import "UserViewController.h"

@interface UserViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblUsername;

@end

@implementation UserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.lblUsername.text = self.username;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
