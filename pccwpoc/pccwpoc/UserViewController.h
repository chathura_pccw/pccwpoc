//
//  UserViewController.h
//  pccwpoc
//
//  Created by Chathura Palihakkara on 16/7/20.
//  Copyright © 2020 PCCW. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserViewController : UIViewController

@property (strong, nonatomic) NSString *username;
@end

NS_ASSUME_NONNULL_END
