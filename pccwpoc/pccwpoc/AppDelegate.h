//
//  AppDelegate.h
//  pccwpoc
//
//  Created by Chathura Palihakkara on 16/7/20.
//  Copyright © 2020 PCCW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : NSObject <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

